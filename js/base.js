function Tree(domObject) {
  this.domObject = domObject; 
}

Tree.prototype.init = function () {

  var this_ = this;
  var parents = this.domObject.find(".parent");

  parents.not(".open").children("ul").hide();
  parents.children("span").on("click", $.proxy(this, "toggle"));

  return this;
};

Tree.prototype.close = function (treeNode) {

  var this_ = this;

  treeNode.removeClass("open").children("ul").slideUp();
  treeNode.find(".parent.open").each(
    function () {
      this_.close($(this));
    }
  );

  return this;
};

Tree.prototype.open = function (treeNode) {

  var this_ = this;

  treeNode.addClass("open").children("ul").slideDown();

  return this;
};

Tree.prototype.toggle = function (e) {

  var clicked = $(e.target).parent();

  if (clicked.hasClass("open")) {
    this.close(clicked);
  } else {
    this.open(clicked);
  }
};

$(document).ready(function () {
  (new Tree($("#menu-tree"))).init();
});